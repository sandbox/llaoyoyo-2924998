<?php

namespace Drupal\misandbox\Controller;

use Drupal\Core\Controller\ControllerBase;

class ExampleController extends ControllerBase {
	/**
	* {@inheritdoc}
	*/	
	public function build(){
		$build = [
			'#type' => 'markup',
			'#markup' => 'Soy el Lucas',
		];

		return $build;	

	}
}
